package com.homework.newsapp.utils

import android.content.Context
import java.io.*

class FileOP {
    fun save(cxt: Context, fileName: String, str: String): Boolean {
        var saveFlag = false
        try {
            val output = cxt.openFileOutput(fileName, Context.MODE_PRIVATE)
            val writer = BufferedWriter(OutputStreamWriter(output))
            writer.use {
                it.write(str)
            }
            saveFlag = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return saveFlag
    }

    fun read(cxt: Context, fileName: String): String {
        val content = StringBuilder()
        try {
            val input = cxt.openFileInput(fileName)
            val reader = BufferedReader(InputStreamReader(input))
            reader.use {
/*                reader.forEachLine {
                    content.append(it)
                }*/
                var line: String = it.readLine() //读取第一行数据
                content.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return content.toString()
    }

    fun readForLogin(cxt: Context, fileName: String): String {
        val content = StringBuilder()
        try {
            val input = cxt.openFileInput(fileName)
            val reader = BufferedReader(InputStreamReader(input))
            reader.use {
/*                reader.forEachLine {
                    content.append(it)
                }*/
                var line: String = it.readLine() //读取第一行数据
                line = it.readLine()  //读取第二行数据
                content.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return content.toString()
    }
}
