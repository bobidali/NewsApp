package com.homework.newsapp.utils

import android.widget.Toast
import android.content.Context
import android.content.Intent

const val Tag: String = "MyApp"

fun myToast(cxt: Context, msg: String, type: Int = 0) {
    val duration = if (type == 1) {
        Toast.LENGTH_LONG
    } else {
        Toast.LENGTH_SHORT
    }
    Toast.makeText(cxt, msg, duration).show()
}

fun goActivity(cxt: Context, cls: Class<*>) {
    val intentObj = Intent(cxt, cls)
    cxt.startActivity(intentObj)
}
