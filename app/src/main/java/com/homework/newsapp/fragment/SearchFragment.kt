package com.homework.newsapp.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.homework.newsapp.DetailActivity
import com.homework.newsapp.R
import com.homework.newsapp.data.NewSearch
import com.homework.newsapp.data.getNewSearch
import kotlinx.android.synthetic.main.fragment_search_layout.*
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okio.IOException

class SearchFragment : Fragment() {
    private lateinit var newsInfos: NewSearch
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.fragment_search_layout, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnEnter_layout.setOnClickListener {
            Toast.makeText(getActivity(), "正在查询...", Toast.LENGTH_SHORT).show()
            getSearchData()
        }
    }

    private fun getSearchData() {
        val okHttpClient = OkHttpClient()
        val url = getNewSearch(txtEnter_layout.text.toString())
        val request = Request.Builder().url(url).build()
        val intent_search = Intent(activity, DetailActivity::class.java)
        okHttpClient.newCall(request).enqueue(object : okhttp3.Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.i("MyAPP", "getNewSearchData Failure" + e.message.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    val json = response.body!!.string()
                    newsInfos = Gson().fromJson(json, NewSearch::class.java)
                    if (newsInfos?.status == 0) {
                        Log.i("MyAPP", "getNewSearchData Success" + newsInfos?.result.toString())
                        val newsInforma = Bundle()
                        val searchNews = newsInfos!!.result.list
                        newsInforma.putString("data_title", searchNews[3].title.toString())
                        newsInforma.putString("data_image", searchNews[3].pic.toString())
                        newsInforma.putString("data_category", searchNews[3].src.toString())
                        newsInforma.putString("data_time", searchNews[3].time.toString())
                        newsInforma.putString("data_content", searchNews[3].content.toString())
                        intent_search.putExtras(newsInforma)
                        startActivity(intent_search)
                    } else {
                        Log.i("MyAPP", newsInfos?.msg)
                    }
                }
            }
        })
    }
}