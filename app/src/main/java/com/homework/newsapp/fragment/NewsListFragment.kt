package com.homework.newsapp.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.homework.newsapp.DetailActivity
import com.homework.newsapp.R
import com.homework.newsapp.adapter.CommonRecyclerAdapter
import com.homework.newsapp.data.*
import kotlinx.android.synthetic.main.item_news_layout.view.*
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class NewsListFragment(private val newsChannel: String) : Fragment() {

    private var newsList: NewsList? = null
    private var newsListData: ArrayList<NewsData>? = ArrayList()

    private var newsListRecyclerView: RecyclerView? = null

    private val NewsListCompleted = 0
    private val NewsListHandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            //    super.handleMessage(msg)
            if (msg.what == NewsListCompleted) {
                newsListRecyclerView!!.adapter = CommonRecyclerAdapter.Builder()
                    .setData(newsListData!!).setLayout(R.layout.item_news_layout)
                    .bindView { holder, innerData, position ->
                        val data = innerData as NewsData
                        holder.itemView.news_title.text = data.title
                        holder.itemView.news_category.text = data.category
                        holder.itemView.news_addTime.text = data.time
                        Glide.with(context!!).load(data.pic)
                            .into(holder.itemView.news_thumb)
                        holder.itemView.news_thumb.setOnClickListener {
                            //    myToast(context!!, data.toString())
                            val newsInfor = Bundle()
                            newsInfor.putString("data_title", data.title.toString())
                            newsInfor.putString("data_image",data.pic.toString())
                            newsInfor.putString("data_category", data.category.toString())
                            newsInfor.putString("data_time", data.time.toString())
                            newsInfor.putString("data_content", data.content.toString())
                            val intent_list = Intent(activity, DetailActivity::class.java)
                            intent_list.putExtras(newsInfor)
                            startActivity(intent_list)
                        }
                    }.create()
                newsListRecyclerView!!.layoutManager = LinearLayoutManager(context!!)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getNewsListData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val cxt: Context? = context
        val view: View = inflater.inflate(R.layout.fragment_newslist_layout, container, false)
        newsListRecyclerView = view.findViewById(R.id.NewsListRecyclerView)
        return view
    }

    private fun getNewsListData() {
        val okHttpClient = OkHttpClient()
        val url = getNewsList(newsChannel)
        val request = Request.Builder().url(url).build()
        okHttpClient.newCall(request).enqueue(object : okhttp3.Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call, response: Response) {
                val json = response.body!!.string()
                newsList = Gson().fromJson(json, NewsList::class.java)
                val msg = Message()
                if (newsList?.status == 0) {
                    //    Log.i("MyNewsApp", "getChanneListData success" + newsList?.result.toString())
                    newsListData = newsList!!.result.list
                    msg.what = NewsListCompleted
                    NewsListHandler.sendMessage(msg)
                } else {
                    Log.i("MyNewsApp", newsList?.msg)
                }
            }
        })
    }
}