package com.homework.newsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val news = intent.extras!!
        titleData_layout.text = news.getString("data_title", "")
        categoryData_layout.text = news.getString("data_category", "")
        timeData_layout.text = news.getString("data_time", "")
        val contentWeb = news.getString("data_content", "")
        val htmlText: Spanned =
            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.N) {
                Html.fromHtml(contentWeb, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(contentWeb)
            }
        contentData_layout.text = htmlText
        val imgweb = news.getString("data_image", "")
        Glide.with(this).load(imgweb).into(imgData_layout)
    }
}