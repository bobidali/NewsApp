package com.homework.newsapp.data

data class NewsChannel(
    val msg: String,
    val result: ArrayList<String>,
    val status: Int
)