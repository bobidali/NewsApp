package com.homework.newsapp.data

val newsBaseUrl = "https://api.jisuapi.com/news/"
val appKey = "8b3f55ffb87900b4"
//val keyword = "姚明"

fun getNewsChannel(): String {
    return newsBaseUrl.plus("channel?appkey=${appKey}")
}

fun getNewsList(channel: String = "头条", start: Int = 0, num: Int = 7): String {
    return newsBaseUrl.plus("get?channel=${channel}&start=${start}&num=${num}&appkey=${appKey}")
}

fun getNewSearch(keyword: String): String {
    return newsBaseUrl.plus("search?keyword=${keyword}&appkey=${appKey}")
}s