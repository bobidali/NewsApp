package com.homework.newsapp.data

data class NewsList(
    val msg: String,
    val result: Result,
    val status: Int
)

data class Result(
    val channel: String,
    val list: ArrayList<NewsData>,
    val num: Int
)

data class NewsData(
    val category: String,
    val content: String,
    val pic: String,
    val src: String,
    val time: String,
    val title: String,
    val url: String,
    val weburl: String
)