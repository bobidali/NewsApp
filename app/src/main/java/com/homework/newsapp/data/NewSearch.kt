package com.homework.newsapp.data

data class NewSearch(
    val status: Int,
    val msg: String,
    val result: ResultCh
)

data class ResultCh(
    val keyword: String,
    val num: Int,
    val list: ArrayList<NewsDataCh>
)

data class NewsDataCh(
    val title: String,
    val time: String,
    val src: String,
    val category: String,
    val pic: String,
    val url: String,
    val weburl: String,
    val content: String
)