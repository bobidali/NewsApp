package com.homework.newsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import com.homework.newsapp.fragment.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val actionBar = supportActionBar
        actionBar?.hide()
        tabBarNews.setOnClickListener(this)
        tabBarSearch.setOnClickListener(this)
        tabBarWrite.setOnClickListener(this)
        changeTab(NewsFragment())
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tabBarNews -> {
                changeTab(NewsFragment())
            }
            R.id.tabBarSearch -> {
                changeTab(SearchFragment())
            }
            R.id.tabBarWrite -> {
                changeTab(WriteFragment())

            }
        }
    }

    private fun changeTab(fragment: Fragment, containerResId: Int = R.id.tabContent) {
        //构建fragment管理器来管理内容的替换！
        supportFragmentManager.beginTransaction().replace(containerResId, fragment).commit()
    }
}